﻿using UnityEngine;
using System.Collections;

public class WeaponManager : MonoBehaviour
{
	public Transform ShootFrom;
	public int NumberLasers = 100;

	public Object LaserPrefab;
	public Material LaserMaterial; // shouldbe on prefab, idk why it isnt being passed on

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			Ray ray = new Ray(transform.position, transform.forward);

			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				GameObject laser = Instantiate(LaserPrefab) as GameObject;
				LaserBehaviour lb = laser.GetComponent<LaserBehaviour>();
				lb.lr.material = LaserMaterial;
				laser.transform.parent = transform;
				lb.Fire(transform.position, hit.point);

				if (hit.collider != null)
				{
					GameObject collider = hit.collider.gameObject;
					HealthManager colliderHealth = collider.GetComponent<HealthManager>();
					if( colliderHealth != null )
					{
						colliderHealth.Hit(15f);
					}
				}
			}
		}
	}
}
