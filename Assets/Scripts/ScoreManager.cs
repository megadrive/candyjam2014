﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
	public HUDNumbers textObject;
	public int Score = 0;
	private int _score = 100;

	void Update()
	{
		if (Score < _score)
		{
			Score++;
		}
		else if (Score > _score)
		{
			Score--;
		}

		textObject.Number = Score;
	}

	public void AddScore(int mod)
	{
		_score += mod;
	}
}
