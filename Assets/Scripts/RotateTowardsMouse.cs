﻿using UnityEngine;

public class RotateTowardsMouse : MonoBehaviour
{
	void Update()
	{
		Plane playerPlane = new Plane(Vector3.up, transform.position);

		// Generate a ray from the cursor position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		float ent;
		if (playerPlane.Raycast(ray, out ent))
		{
			Vector3 hitPoint = ray.GetPoint(ent);
			transform.LookAt(hitPoint);
		}
	}
}
