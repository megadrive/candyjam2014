﻿using UnityEngine;

public class LaserBehaviour : MonoBehaviour
{
	public float liveTime = 0.2f;
	float timer = 0.0f;

	[HideInInspector]
	public LineRenderer lr;

	Color colstart = Color.red;
	Color colend = Color.red;

	bool _available = false;
	public bool IsAvailable { get { return _available; } }

	public Material laserMaterial;

	public Transform parent;

	void Awake()
	{
		lr = gameObject.AddComponent<LineRenderer>();
		gameObject.transform.parent = parent;
	}

	void Update()
	{
		if (timer < liveTime)
		{
			timer += Time.deltaTime;

			// update alpha
			var a = 1.0f - (timer / liveTime);
			var sAlpha = colstart;
			var eAlpha = colend;
			sAlpha.a = a;
			eAlpha.a = a;

			lr.SetColors(sAlpha, eAlpha);
		}
		else
		{
			//TODO: Make work without instantiating
			//timer = 0.0f;
			//_available = true;
			Destroy(gameObject);
		}
	}

	public void Fire(Vector3 start, Vector3 end)
	{
		timer = 0.0f;
		lr.SetColors(colstart, colend);
		lr.SetPosition(0, start);
		lr.SetPosition(1, end);
	}
}
