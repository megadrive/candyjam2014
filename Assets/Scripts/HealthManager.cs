﻿using UnityEngine;
using System.Collections;

public class HealthManager : MonoBehaviour
{
	float _health;
	public float CurrentHealth;

	public float Health = 30f;

	public ScoreManager scoreManager;

	// Use this for initialization
	void Start()
	{
		_health = CurrentHealth = Health;

		scoreManager = GameObject.FindGameObjectWithTag("Player").GetComponent<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update()
	{
		CurrentHealth = _health; // just for inspector viewing
	}

	public void Hit(float HealthToRemove)
	{
		_health -= HealthToRemove;

		if (_health <= 0f)
		{
			scoreManager.AddScore(10);
			Destroy(gameObject);
		}
	}

	public void MakeAlive()
	{
		CurrentHealth = Health;
	}
}
