﻿using UnityEngine;
using System.Collections;

public class KeyboardMovement : MonoBehaviour
{
	string h_axis = "Horizontal";
	string v_axis = "Vertical";

	public float Speed;
	Vector3 _velocity;

	Rigidbody _rb;

	// Use this for initialization
	void Start()
	{
		_rb = transform.rigidbody;
	}
	
	// Update is called once per frame
	void Update()
	{
		var xSpeed = Input.GetAxis(h_axis);
		var zSpeed = Input.GetAxis(v_axis);

		_velocity = new Vector3(-(zSpeed * Speed), 0, xSpeed * Speed);
	}

	void FixedUpdate()
	{
		_rb.velocity = _velocity;
	}
}
