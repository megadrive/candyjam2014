﻿using UnityEngine;
using System.Collections;

public class MoveTowardsTransform : MonoBehaviour {

	public Transform MoveTowards;
	public float Speed = 0.1f;

	Vector3 dirTowards;

	// Use this for initialization
	void Start () {
		// get player prefab
		MoveTowards = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update()
	{
		dirTowards = (((MoveTowards.position - transform.position).normalized) * Speed) * Time.deltaTime;

		transform.LookAt(MoveTowards);
	}

	void FixedUpdate()
	{
		transform.rigidbody.MovePosition(transform.position + dirTowards);
	}
}
