﻿using UnityEngine;
using System.Collections;

public class LaserPool
{
	LaserBehaviour[] _lasers;
	private int _numberOfLasers;

	public LaserPool(GameObject laserPrefab, int NumberOfLasers)
	{
		_numberOfLasers = NumberOfLasers <= 0 ? 100 : NumberOfLasers; // 100 as default

		_lasers = new LaserBehaviour[_numberOfLasers];
		for (int i = 0; i < _numberOfLasers; ++i)
		{
			//_lasers[i] = GameObject.Instantiate();
			//_lasers[i]
		}
	}

	public LaserBehaviour GetFirstAvailable()
	{
		foreach (LaserBehaviour l in _lasers)
		{
			if (l.IsAvailable)
			{
				return l;
			}
		}

		return null;
	}
}
