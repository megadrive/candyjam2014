﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{
	public Transform Parent;
	public GameObject EnemyPrefab;

	public float SpawnFrequency = 10f;
	float _spawnTimer = 11f;

	public int Waves = 0;

	public enum Status
	{
		Start, Stop
	}

	public Status status = Status.Start;

	// Use this for initialization
	void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
		if (status == Status.Start)
		{
			_spawnTimer += Time.deltaTime;

			if (_spawnTimer > SpawnFrequency)
			{
				GameObject go = Instantiate(EnemyPrefab, transform.position, transform.rotation) as GameObject;
				_spawnTimer = 0.0f;

				go.transform.parent = Parent;

				Waves++;
			}
		}
		//else // Status.Stop
		//{
		//	GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Enemy");
		//	foreach (GameObject go in gameObjects)
		//	{
		//		Destroy(go);
		//	}
		//}
	}
}
